#include "../inc/main.h"

void test_insert()
{
    printf("%s------------%s\n", WHITE, RESET);
    printf("TEST_INSERT %s|%s\n", WHITE, RESET);
    printf("%s------------%s\n", WHITE, RESET);

    double tdiff;
    char *Titles[nMovies] = {"The Shining", "A Nightmare on Elm Street", "The House That Jack Built", "Silent Hill", "The Illusionist"};
    char *Families[10] = {"Ivanov", "Sokolova", "Churikova", "Pinchuk", "Shapovalova", "Mikhailov", "Don", "Yeager", "Moroz", "Caprio"};
    int i;

    initGraphsData("data.cfg");

    memDBScheme *Scheme = createScheme();

    nodeStruct *MovieNodeType = addNodeTypeToScheme(Scheme, "Movie");
    addAttrToNodeScheme(MovieNodeType, "Title", aString);
    addAttrToNodeScheme(MovieNodeType, "Year", aInt32);

    memDB *DB = createNew(Scheme, "graphs.mydb");
    clock_t tstart;
    clock_t tend;

    for (int j = 0; j < 10; j++)
    {
        tstart = clock();

        for (i = 0; i < 1000; i++)
        {
            createNode(DB, MovieNodeType);
            setNodeAttr(DB, MovieNodeType, "Title", createString(DB, Titles[i % 5]));
            setNodeAttr(DB, MovieNodeType, "Year", 2000 + i);
            postNode(DB, MovieNodeType);
        }
        tend = clock();
        tdiff = (double)(tend - tstart) / CLOCKS_PER_SEC;
        printf("Операция вставки %d строк заняла %f секунд\n", 1000 * j, tdiff);
    }

    rewindFirstNodes(DB, MovieNodeType);
    i = 0;
    while (openNode(DB, MovieNodeType))
    {
        int Year;
        char *Title;
        Year = getNodeAttr(DB, MovieNodeType, "Year");
        Title = getString(DB, getNodeAttr(DB, MovieNodeType, "Title"));
        register_free(1 + strlen(Title));
        free(Title);
        nextNode(DB, MovieNodeType);
        i++;
    }
    printf("Здесь %i Movies\n", i);

    printf("Очищаю предыдущие значения..\n");
    deleteCypherStyle(DB, 1, MovieNodeType, NULL);
    printf("Успешно\n");
    
    closeDB(DB);
    if (getOccupiedMemory() == 0)
        printf("Память корректно освобождена!\n");
    else
        printf("Осталось: %i байтов!\n", getOccupiedMemory());

}

void test_select_and_update()
{
    printf("%s----------------------%s\n", WHITE, RESET);
    printf("\n\nTEST_SELECT_AND_UPDATE%s|%s\n", WHITE, RESET);
    printf("%s----------------------%s\n", WHITE, RESET);
    double start;

    char *Titles[5] = {"The Shining", "A Nightmare on Elm Street", "The House That Jack Built", "Silent Hill", "The Illusionist"};
    char *Families[10] = {"Ivanov", "Sokolova", "Churikova", "Pinchuk", "Shapovalova", "Mikhailov", "Don", "Yeager", "Moroz", "Caprio"};
    int i;

    initGraphsData("data.cfg");

    memDBScheme *Scheme = createScheme();

    nodeStruct *MovieNodeType = addNodeTypeToScheme(Scheme, "Movie");
    addAttrToNodeScheme(MovieNodeType, "Title", aString);
    addAttrToNodeScheme(MovieNodeType, "Year", aInt32);

    memDB *DB = createNew(Scheme, "graphs.mydb");

    memCondition *cond = createIntOrBoolAttrCondition(opLess, "Year", 2004);
    for (i = 0; i < nMovies * 4000; i++)
    {
        char *Title = gen_rand_str();
        if (i % 4000 == 0)
        {
            start = seconds();

            memNodeSetItem *ns = queryCypherStyle(DB, 1, MovieNodeType, cond);
            
            freeNodeSet(DB, ns);

            printf("Операция выбора %d строк заняла %lf секунд\n", i, (seconds() - start));

            start = seconds();

            setCypherStyle(DB, "Year", 1975, 1, MovieNodeType, cond);

            printf("Операция обновления %d строк заняла %lf секунд\n", i, (seconds() - start));
        }
        createNode(DB, MovieNodeType);
        setNodeAttr(DB, MovieNodeType, "Title", createString(DB, Title));
        free(Title);
        setNodeAttr(DB, MovieNodeType, "Year", 2000 + i);
        postNode(DB, MovieNodeType);
    }

    printf("Очищаю предыдущие значения..\n");
    deleteCypherStyle(DB, 1, MovieNodeType, NULL);
    printf("Успешно\n");

    closeDB(DB);
    freeCondition(cond);
    
    if (getOccupiedMemory() == 0)
        printf("Память корректно освобождена!\n");
    else
        printf("Осталось: %i байтов!\n", getOccupiedMemory());

}

void test_delete()
{
    printf("%s-----------%s\n", WHITE, RESET);
    printf("\n\nTEST_DELETE%s|%s\n", WHITE, RESET);
    printf("%s-----------%s\n", WHITE, RESET);
    double start;

    char *Titles[5] = {"The Shining", "A Nightmare on Elm Street", "The House That Jack Built", "Silent Hill", "The Illusionist"};
    char *Families[10] = {"Ivanov", "Sokolova", "Churikova", "Pinchuk", "Shapovalova", "Mikhailov", "Don", "Yeager", "Moroz", "Caprio"};
    int i;

    initGraphsData("data.cfg");

    memDBScheme *Scheme = createScheme();

    nodeStruct *MovieNodeType = addNodeTypeToScheme(Scheme, "Movie");
    addAttrToNodeScheme(MovieNodeType, "Title", aString);
    addAttrToNodeScheme(MovieNodeType, "Year", aInt32);

    memDB *DB = createNew(Scheme, "graphs.mydb");

    memCondition *cond = createIntOrBoolAttrCondition(opLess, "Year", 2004);

    for (int j = 0; j < nMovies; j++) {
        for (i = 0; i < j*4000; i++) {
            char * Title = gen_rand_str();
            createNode(DB, MovieNodeType);
            setNodeAttr(DB, MovieNodeType, "Title", createString(DB, Title));
            free(Title);
            setNodeAttr(DB, MovieNodeType, "Year", 2000 + i);
            postNode(DB, MovieNodeType);
        }
        start = seconds();

        deleteCypherStyle(DB, 1, MovieNodeType, cond);

        printf("Операция удаления %i строк заняла %lf секунд\n", j*4000, (seconds() - start));
    }

    printf("Очищаю предыдущие значения..\n");
    deleteCypherStyle(DB, 1, MovieNodeType, NULL);
    printf("Успешно\n");

    closeDB(DB);
    freeCondition(cond);
    
    if (getOccupiedMemory() == 0)
        printf("Память корректно освобождена!\n");
    else
        printf("Осталось: %i байтов!\n", getOccupiedMemory());

}

int main()
{
    test_insert();
    test_select_and_update();
    test_delete();
    return 0;
}