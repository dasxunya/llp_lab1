#include "../inc/test_data.h"
#include "../inc/graph_op.h"
int BufferSize = 1024*32; //у каждого узла имеется буфер
int relink_table_delta = 20; // добавка
int reserved_for_links_in_node = 8; 
int occupied_memory = 0;

//открытие файла с входными значениями
void initGraphsData(char * configFileName) {
    FILE * F = fopen(configFileName, "rt");
    if (F) {
        char buf[512];
        while (!feof(F)) {
            if (fgets(buf, sizeof(buf), F) && strlen(buf) != 0 && strcmp(buf, "\n") != 0) {
                char * str = buf;
                char * name = str;
                while (*str != 0 && *str != '=' && *str != ' ') str++;
                if (*str == 0)
                    printf("Config: unrecognized line : %s\n", name);
                else {
                    char * val = str+1;
                    int ival;
                    *str = 0;
                    while (*val != 0 && (*val == '=' || *val == ' ')) val++;
                    sscanf(val, "%i", &ival);
                    if (strcmp(name, "BufferSize") == 0)
                        BufferSize = ival;
                    else if (strcmp(name, "relink_table_delta") == 0)
                        relink_table_delta = ival;
                    else if (strcmp(name, "reserved_for_links_in_node") == 0)
                        reserved_for_links_in_node = ival;
                    else
                        printf("Config: unrecognized var : %s\n", name);
                }
            }
        }
        fclose(F);
    }
}

//работа с памятью
void register_free(int amount) {
    occupied_memory -= amount;
}


int getOccupiedMemory() {
    return occupied_memory;
}

//перефомирование указателей
typedef struct {
    nodeStruct ** place;
    nodeStruct * old_value; // старое значение (загруженное)
    nodeStruct * new_value; // новое значение
} relink_item;

void addRelinking(nodeStruct ** place, nodeStruct * old_value, nodeStruct * new_value,
                  relink_item ** relink_table, int * relink_table_size, int * relink_table_capacity) {
    if (*relink_table_size == *relink_table_capacity) {
        *relink_table_capacity += relink_table_delta;
        *relink_table = (relink_item *) realloc(*relink_table, (*relink_table_capacity)*sizeof(relink_item));
    }
    (*relink_table)[*relink_table_size].place = place;
    (*relink_table)[*relink_table_size].old_value = old_value;
    (*relink_table)[*relink_table_size].new_value = new_value;
    (*relink_table_size)++;
}

// create new DB
memDBScheme * createScheme() {
    memDBScheme * result = (memDBScheme *) malloc(sizeof(memDBScheme));
    occupied_memory += sizeof(memDBScheme);
    result->FirstSchemeNode = NULL;
    result->LastSchemeNode = NULL;
    return result;
}

void make_relinkings(relink_item ** relink_table, int * relink_table_size, int * relink_table_capacity) {
    int i;
    for (i = 0; i < *relink_table_size; i++)
        if ((*relink_table)[i].place != NULL) {
            int j = 0;
            while (j < *relink_table_size && (*relink_table)[j].place != NULL || (*relink_table)[j].old_value != (*relink_table)[i].old_value)
                j++;
            *(*relink_table)[i].place = (*relink_table)[j].new_value;
        }
}

void write_buffer(char * Buffer, int * filledBuffer, float What) {
    char * what = (char *) &What;
    int i;
    Buffer += *filledBuffer;
    for (i = 0; i < sizeof(float); i++, Buffer++, (*filledBuffer)++)
        *Buffer = what[i];
}

void freeDBSchemeAttr(memoryAttr * Attr) {
    occupied_memory -= strlen(Attr->attrName)+1;
    free(Attr->attrName);
    occupied_memory -= sizeof(memoryAttr);
    free(Attr);
}

void freeDBSchemeNode(nodeStruct * NodeScheme) { 
    nodeConnection * directed = NodeScheme->connectFirst;
    memoryAttr * attr = NodeScheme->firstFromAttrs;
    occupied_memory -= 1 + strlen(NodeScheme->nameType);
    free(NodeScheme->nameType);
    while (directed != NULL) {
        nodeConnection * to_delete = directed;
        directed = directed->next;
        occupied_memory -= sizeof(nodeConnection);
        free(to_delete);
    }
    while (attr != NULL) {
        memoryAttr * to_delete = attr;
        attr = attr->next;
        freeDBSchemeAttr(to_delete);
    }
    occupied_memory -= BufferSize;
    free(NodeScheme->buffer);
    occupied_memory -= sizeof(nodeStruct);
    free(NodeScheme);
}

void freeDBScheme(memDBScheme * Scheme) { 
    nodeStruct * result = Scheme->FirstSchemeNode;
    while (result != NULL) {
        nodeStruct * to_delete = result;
        result = result->NextNodeStruct;
        freeDBSchemeNode(to_delete);
    }
    occupied_memory -= sizeof(memDBScheme);
    free(Scheme);
}

nodeStruct * findNodeSchemeByTypeName(memDBScheme * Scheme, char * TypeName, int * n) {
    nodeStruct * result = Scheme->FirstSchemeNode;
    *n = 0;
    while (result != NULL)
        if (strcmp(TypeName, result->nameType) == 0)
            return result;
        else {
            result = result->NextNodeStruct;
            (*n)++;
        }
    *n = -1;
    return NULL;
}

nodeStruct * addNodeTypeToScheme(memDBScheme * Scheme, char * TypeName) { 
    nodeStruct * result;
    if (Scheme->FirstSchemeNode == NULL || Scheme->LastSchemeNode == NULL) {
        result = (nodeStruct *) malloc(sizeof(nodeStruct));
        occupied_memory += sizeof(nodeStruct);
        Scheme->FirstSchemeNode = result;
        Scheme->LastSchemeNode = result;
    } else {
        int n;
        if (findNodeSchemeByTypeName(Scheme, TypeName, &n)) // если нашли
            return NULL;
        result = (nodeStruct *) malloc(sizeof(nodeStruct));
        occupied_memory += sizeof(nodeStruct);
        Scheme->LastSchemeNode->NextNodeStruct = result;
        Scheme->LastSchemeNode = result;
    }
    result->nameType = (char *) malloc(1 + strlen(TypeName)*sizeof(char));
    occupied_memory += 1 + strlen(TypeName)*sizeof(char);
    strcpy(result->nameType, TypeName);
    result->rootOffset = 0;
    result->FirstElOffset = 0;
    result->lastElOffset = 0;
    result->buffer = (char *) malloc(BufferSize*sizeof(char)); 
    occupied_memory += BufferSize*sizeof(char);
    result->filledBuffer = 0; 
    result->created = 0;
    result->prevNodeOffset = 0; 
    result->thisNodeOffset = 0; 
    result->connectFirst = NULL;
    result->connectLast = NULL;
    result->firstFromAttrs = NULL;
    result->lastFromAttrs = NULL;
    result->NextNodeStruct = NULL;

    return result;
}

void delNodeTypeFromScheme(memDBScheme * Scheme, nodeStruct * NodeScheme) {
    if (Scheme->FirstSchemeNode != NULL && Scheme->LastSchemeNode != NULL) {
        if (Scheme->FirstSchemeNode == Scheme->LastSchemeNode) {
            if (Scheme->FirstSchemeNode == NodeScheme) {
                freeDBSchemeNode(NodeScheme);
                Scheme->FirstSchemeNode = NULL;
                Scheme->LastSchemeNode = NULL;
            }
        } else if (Scheme->FirstSchemeNode == NodeScheme) {
            Scheme->FirstSchemeNode = NodeScheme->NextNodeStruct;
            freeDBSchemeNode(NodeScheme);
        } else {
            nodeStruct * prev = Scheme->FirstSchemeNode;
            while (prev != NULL && prev->NextNodeStruct != NodeScheme)
                prev = prev->NextNodeStruct;
            if (prev != NULL) {
                if (Scheme->LastSchemeNode == NodeScheme) {
                    Scheme->LastSchemeNode = prev;
                    prev->NextNodeStruct = NULL;
                } else {
                    prev->NextNodeStruct = NodeScheme->NextNodeStruct;
                }
                freeDBSchemeNode(NodeScheme);
            }
        }
    }
}

nodeConnection * checkCanLinkTo(nodeStruct * NodeScheme, nodeStruct * ToNodeScheme) {
    nodeConnection * result = NodeScheme->connectFirst;
    while (result != NULL)
        if (ToNodeScheme == result->node)
            return result;
        else
            result = result->next;
    return NULL;
}


nodeConnection * addDirectedToNodeScheme(nodeStruct * NodeScheme, nodeStruct * ToNodeScheme) {
    nodeConnection * rec;
    if (checkCanLinkTo(NodeScheme, ToNodeScheme))
        return NULL;
    rec = (nodeConnection *) malloc(sizeof(nodeConnection));
    occupied_memory += sizeof(nodeConnection);
    rec->node = ToNodeScheme;
    rec->next = NULL;
    if (NodeScheme->connectFirst == NULL || NodeScheme->connectLast == NULL) {
        NodeScheme->connectFirst = rec;
        NodeScheme->connectLast = rec;
    } else {
        NodeScheme->connectLast->next = rec;
        NodeScheme->connectLast = rec;
    }
    return rec;
}

void delDirectedToFromNodeType(nodeStruct * NodeScheme, nodeStruct * Deleting) {
    if (NodeScheme->connectFirst != NULL && NodeScheme->connectLast != NULL) {
        if (NodeScheme->connectFirst == NodeScheme->connectLast) {
            if (NodeScheme->connectFirst->node == Deleting) {
                occupied_memory -= sizeof(nodeConnection);
                free(NodeScheme->connectFirst);
                NodeScheme->connectFirst = NULL;
                NodeScheme->connectLast = NULL;
            }
        } else if (NodeScheme->connectFirst->node == Deleting) {
            nodeConnection * deleted = NodeScheme->connectFirst;
            NodeScheme->connectFirst = NodeScheme->connectFirst->next;
            occupied_memory -= sizeof(nodeConnection);
            free(deleted);
        } else {
            nodeConnection * prev = NodeScheme->connectFirst;
            while (prev != NULL && prev->next->node != Deleting)
                prev = prev->next;
            if (prev != NULL) {
                nodeConnection * deleted = prev->next;
                if (NodeScheme->connectLast->node == Deleting) {
                    NodeScheme->connectLast = prev;
                    prev->next = NULL;
                } else {
                    prev->next = prev->next->next;
                }
                occupied_memory -= sizeof(nodeConnection);
                free(deleted);
            }
        }
    }
}

memoryAttr * findAttrByName(nodeStruct * NodeScheme, char * Name, int * n) {
    memoryAttr * result = NodeScheme->firstFromAttrs;
    *n = 0;
    while (result != NULL)
        if (strcmp(Name, result->attrName) == 0)
            return result;
        else {
            result = result->next;
            (*n)++;
        }
    *n = -1;
    return NULL;
}


memoryAttr * addAttrToNodeScheme(nodeStruct * NodeScheme, char * Name, unsigned char Type) {
    memoryAttr * result;
    if (NodeScheme->firstFromAttrs == NULL || NodeScheme->lastFromAttrs == NULL) {
        result = (memoryAttr *) malloc(sizeof(memoryAttr));
        occupied_memory += sizeof(memoryAttr);
        NodeScheme->firstFromAttrs = result;
        NodeScheme->lastFromAttrs = result;
    } else {
        
        int n; 
        if (findAttrByName(NodeScheme, Name, &n)) // если нашли
            return NULL;
        result = (memoryAttr *) malloc(sizeof(memoryAttr));
        occupied_memory += sizeof(memoryAttr);
        NodeScheme->lastFromAttrs->next = result;
        NodeScheme->lastFromAttrs = result;
    }
    result->attrName = (char *) malloc(1 + strlen(Name)*sizeof(char));
    occupied_memory += 1 + strlen(Name)*sizeof(char);
    strcpy(result->attrName, Name);
    result->type = Type;
    result->next = NULL;

    return result;
}

void delAttrFromNodeScheme(nodeStruct * NodeScheme, memoryAttr * Deleting) {
    if (NodeScheme->firstFromAttrs != NULL && NodeScheme->lastFromAttrs != NULL) {
        occupied_memory -= 1 + strlen(Deleting->attrName);
        free(Deleting->attrName);
        if (NodeScheme->firstFromAttrs == NodeScheme->lastFromAttrs) {
            if (NodeScheme->firstFromAttrs == Deleting) {
                occupied_memory -= sizeof(memoryAttr);
                free(NodeScheme->firstFromAttrs);
                NodeScheme->firstFromAttrs = NULL;
                NodeScheme->lastFromAttrs = NULL;
            }
        } else if (NodeScheme->firstFromAttrs == Deleting) {
            memoryAttr * deleted = NodeScheme->firstFromAttrs;
            NodeScheme->firstFromAttrs = NodeScheme->firstFromAttrs->next;
            occupied_memory -= sizeof(memoryAttr);
            free(deleted);
        } else {
            memoryAttr * prev = NodeScheme->firstFromAttrs;
            while (prev != NULL && prev->next != Deleting)
                prev = prev->next;
            if (prev != NULL) {
                memoryAttr * deleted = prev->next;
                if (NodeScheme->lastFromAttrs == Deleting) {
                    NodeScheme->lastFromAttrs = prev;
                    prev->next = NULL;
                } else {
                    prev->next = prev->next->next;
                }
                occupied_memory -= sizeof(memoryAttr);
                free(deleted);
            }
        }
    }
}

void db_fwrite(void * buf, int item_size, int n_items, memDB * DB) {
    char * _buf = (char *) buf;
    int n_free = BufferSize - DB->nWriteBuffer;
    int n_bytes = item_size*n_items;
    int i = DB->nWriteBuffer;
    if (DB->iReadBuffer < DB->nReadBuffer) {
        fseek(DB->FileDB, DB->iReadBuffer - DB->nReadBuffer, SEEK_CUR);
        DB->iReadBuffer = 0;
        DB->nReadBuffer = 0;
    }
    if (n_free > 0) {
        int to_write = n_free < n_bytes ? n_free : n_bytes;
        DB->nWriteBuffer += to_write;
        n_bytes -= to_write;
        for ( ; to_write > 0; to_write--, i++)
            DB->WriteBuffer[i] = *_buf++;
    }
    if (DB->nWriteBuffer == BufferSize) {
        fwrite(DB->WriteBuffer, 1, BufferSize, DB->FileDB);
        fwrite(_buf, 1, n_bytes, DB->FileDB);
        DB->nWriteBuffer = 0;
    }
}

void db_fread(void * buf, int item_size, int n_items, memDB * DB) {
    char * _buf = (char *) buf;
    int n_have = DB->nReadBuffer - DB->iReadBuffer;
    int n_bytes = item_size*n_items;
    for ( ; n_bytes > 0 && n_have > 0; n_have--, n_bytes--)
        *_buf++ = DB->ReadBuffer[DB->iReadBuffer++];
    if (n_bytes > 0) {
        fread(_buf, 1, n_bytes, DB->FileDB);
        DB->iReadBuffer = 0;
        DB->nReadBuffer = fread(DB->ReadBuffer, 1, BufferSize, DB->FileDB);
    }
}

void db_fflush(memDB * DB) {
    if (DB->iReadBuffer < DB->nReadBuffer) {
        fseek(DB->FileDB, DB->iReadBuffer - DB->nReadBuffer, SEEK_CUR);
        DB->iReadBuffer = 0;
        DB->nReadBuffer = 0;
    }
    if (DB->nWriteBuffer > 0) {
        fwrite(DB->WriteBuffer, 1, DB->nWriteBuffer, DB->FileDB);
        fflush(DB->FileDB);
        DB->nWriteBuffer = 0;
    }
}

int db_feof(memDB * DB) {
    db_fflush(DB);
    return feof(DB->FileDB);
}

void db_fclose(memDB * DB) {
    db_fflush(DB);
    fclose(DB->FileDB);
}

long int db_ftell(memDB * DB) {
    db_fflush(DB);
    return ftell(DB->FileDB);
}

void db_fseek(memDB * DB, long int offset, int whence) {
    db_fflush(DB);
    fseek(DB->FileDB, offset, whence);
}

long int getDBSize(memDB * DB) {
    db_fseek(DB, 0, SEEK_END);
    return db_ftell(DB);
}


void storeDirectedList(memDB * DB, nodeConnection * List) {
    while (List != NULL) {
        db_fwrite(&List->node, sizeof(List->node), 1, DB);
        List = List->next;
    }
    db_fwrite(&List, sizeof(List), 1, DB);
}


void storeAttrsList(memDB * DB, memoryAttr * List) {
    int NameStringLength;
    while (List != NULL) {
        NameStringLength = 1 + strlen(List->attrName);
        db_fwrite(&NameStringLength, sizeof(NameStringLength), 1, DB);
        db_fwrite(List->attrName, NameStringLength, 1, DB);
        db_fwrite(&List->type, sizeof(List->type), 1, DB);
        List = List->next;
    }
    NameStringLength = 0;
    db_fwrite(&NameStringLength, sizeof(NameStringLength), 1, DB);
}

void storeNodeScheme(memDB * DB, nodeStruct * NodeScheme) {
    int TypeStringLength = 1 + strlen(NodeScheme->nameType);
    db_fwrite(&NodeScheme, sizeof(NodeScheme), 1, DB);
    db_fwrite(&TypeStringLength, sizeof(TypeStringLength), 1, DB);
    db_fwrite(NodeScheme->nameType, TypeStringLength, 1, DB);
    storeDirectedList(DB, NodeScheme->connectFirst);
    storeAttrsList(DB, NodeScheme->firstFromAttrs);
}


void storeScheme(memDB * DB, memDBScheme * Scheme) {
    nodeStruct * NodeScheme = Scheme->FirstSchemeNode;
    while (NodeScheme != NULL) {
        storeNodeScheme(DB, NodeScheme);
        NodeScheme = NodeScheme->NextNodeStruct;
    }
    db_fwrite(&NodeScheme, sizeof(NodeScheme), 1, DB);
    NodeScheme = Scheme->FirstSchemeNode;
    while (NodeScheme != NULL) {
        int EmptyOffset = 0;
        NodeScheme->rootOffset = db_ftell(DB);
        db_fwrite(&EmptyOffset, sizeof(EmptyOffset), 1, DB);
        db_fwrite(&EmptyOffset, sizeof(EmptyOffset), 1, DB);
        NodeScheme = NodeScheme->NextNodeStruct;
    }
}


void loadAttrsList(memDB * DB, nodeStruct * NodeScheme) {
    int NameStringLength;
    char * NameString;
    unsigned char Type;
    do {
        db_fread(&NameStringLength, sizeof(NameStringLength), 1, DB);
        if (NameStringLength > 0) {
            NameString = (char *) malloc(NameStringLength*sizeof(char));
            occupied_memory += NameStringLength*sizeof(char);
            db_fread(NameString, NameStringLength, 1, DB);
            db_fread(&Type, sizeof(Type), 1, DB);
            addAttrToNodeScheme(NodeScheme, NameString, Type);
            occupied_memory -= NameStringLength;
            free(NameString);
        }
    } while (NameStringLength != 0);
}


void loadDirectedList(memDB * DB, nodeStruct * NodeScheme, relink_item ** relink_table, int * relink_table_size, int * relink_table_capacity) {
    nodeStruct * loadedNodeScheme;
    do {
        db_fread(&loadedNodeScheme, sizeof(loadedNodeScheme), 1, DB);
        if (loadedNodeScheme) {
            nodeConnection * rec = addDirectedToNodeScheme(NodeScheme, loadedNodeScheme);
            addRelinking(&rec->node, loadedNodeScheme, NULL, relink_table, relink_table_size, relink_table_capacity);
        }
    } while (loadedNodeScheme != NULL);
}


nodeStruct * loadNodeScheme(memDB * DB, relink_item ** relink_table, int * relink_table_size, int * relink_table_capacity) {
    nodeStruct * loadedNodeScheme;
    nodeStruct * NodeScheme;
    char * TypeString;
    int TypeStringLength;
    db_fread(&loadedNodeScheme, sizeof(loadedNodeScheme), 1, DB);
    if (loadedNodeScheme == NULL) return NULL;

    db_fread(&TypeStringLength, sizeof(TypeStringLength), 1, DB);
    TypeString = (char *) malloc(TypeStringLength*sizeof(char));
    occupied_memory += TypeStringLength*sizeof(char);
    db_fread(TypeString, TypeStringLength, 1, DB);
    NodeScheme = addNodeTypeToScheme(DB->Scheme, TypeString);
    occupied_memory -= TypeStringLength;
    free(TypeString);
    addRelinking(NULL, loadedNodeScheme, NodeScheme, relink_table, relink_table_size, relink_table_capacity);
    loadDirectedList(DB, NodeScheme, relink_table, relink_table_size, relink_table_capacity);
    loadAttrsList(DB, NodeScheme);
    return NodeScheme;
}


void loadScheme(memDB * DB, memDBScheme * Scheme) {
    nodeStruct * NodeScheme;
    relink_item * relink_table = (relink_item *) malloc(relink_table_delta*sizeof(relink_item));
    occupied_memory += relink_table_delta*sizeof(relink_item);
    int relink_table_capacity = relink_table_delta;
    int relink_table_size = 0;
    while (loadNodeScheme(DB, &relink_table, &relink_table_size, &relink_table_capacity));
    make_relinkings(&relink_table, &relink_table_size, &relink_table_capacity);
    occupied_memory -= relink_table_capacity*sizeof(relink_item);
    free(relink_table);
    NodeScheme = Scheme->FirstSchemeNode;
    while (NodeScheme != NULL) {
        NodeScheme->rootOffset = db_ftell(DB);
        db_fread(&NodeScheme->FirstElOffset, sizeof(NodeScheme->FirstElOffset), 1, DB);
        db_fread(&NodeScheme->lastElOffset, sizeof(NodeScheme->lastElOffset), 1, DB);
        NodeScheme->thisNodeOffset = NodeScheme->FirstElOffset;
        NodeScheme = NodeScheme->NextNodeStruct;
    }
}


memDB * createNew(memDBScheme * Scheme, char * FileName) {
    memDB * result = (memDB *) malloc(sizeof(memDB));
    occupied_memory += sizeof(memDB);
    result->FileDB = fopen(FileName, "w+b");
    if (result->FileDB) {
        long int SchemeLength;
        result->Scheme = Scheme;
        result->WriteBuffer = (char *) malloc(BufferSize);
        occupied_memory += BufferSize;
        result->nWriteBuffer = 0;
        result->ReadBuffer = (char *) malloc(BufferSize);
        occupied_memory += BufferSize;
        result->nReadBuffer = 0;
        result->iReadBuffer = 0;
        db_fseek(result, sizeof(long int), SEEK_SET); 
        storeScheme(result, Scheme);
        SchemeLength = db_ftell(result);
        db_fseek(result, 0, SEEK_SET);
        db_fwrite(&SchemeLength, sizeof(SchemeLength), 1, result); 
        db_fflush(result);
        return result;
    } else {
        occupied_memory -= sizeof(*result);
        free(result);
        return NULL;
    }
}

void closeDB(memDB * DB) {
    freeDBScheme(DB->Scheme);
    db_fclose(DB);
    free(DB->WriteBuffer);
    free(DB->ReadBuffer);
    occupied_memory -= 2*BufferSize;
    free(DB);
    occupied_memory -= sizeof(*DB);
}


memDB * openDB(char * FileName) {
    memDB * result = (memDB *) malloc(sizeof(memDB));
    occupied_memory += sizeof(memDB);
    result->FileDB = fopen(FileName, "r+b");
    if (result->FileDB) {
        result->Scheme = createScheme();
        result->WriteBuffer = (char *) malloc(BufferSize);
        occupied_memory += BufferSize;
        result->nWriteBuffer = 0;
        result->ReadBuffer = (char *) malloc(BufferSize);
        occupied_memory += BufferSize;
        result->nReadBuffer = 0;
        result->iReadBuffer = 0;
        db_fseek(result, sizeof(long int), SEEK_SET);
        loadScheme(result, result->Scheme);
        return result;
    } else {
        occupied_memory -= sizeof(*result);
        free(result);
        return NULL;
    }
}

void createNode(memDB * DB, nodeStruct * NodeScheme) {
    memoryAttr * AList = NodeScheme->firstFromAttrs;
    int i;
    cancelNode(DB, NodeScheme);
    NodeScheme->filledBuffer = 0;
    NodeScheme->created = 1;

    while (AList != NULL) {
        write_buffer(NodeScheme->buffer, &NodeScheme->filledBuffer, 0.0);
        AList = AList->next;
    }
    write_buffer(NodeScheme->buffer, &NodeScheme->filledBuffer, 0.0);
    
    for (i = 0; i < reserved_for_links_in_node; i++) {
        write_buffer(NodeScheme->buffer, &NodeScheme->filledBuffer, 0.0);
        write_buffer(NodeScheme->buffer, &NodeScheme->filledBuffer, 0.0);
    }
}

int openNode(memDB * DB, nodeStruct * NodeScheme) {
    unsigned char Type;
    int Dummy;
    int n;
    cancelNode(DB, NodeScheme);
    if (NodeScheme->thisNodeOffset == 0)
        return 0;
    NodeScheme->filledBuffer = 0;
    NodeScheme->created = 0;
    db_fseek(DB, NodeScheme->thisNodeOffset, SEEK_SET);
    db_fread(&n, sizeof(n), 1, DB);
    db_fread(&Type, sizeof(Type), 1, DB);
    if (Type != fNode)
        return 0;
    db_fread(&Dummy, sizeof(int), 1, DB);
    NodeScheme->filledBuffer = n - sizeof(n) - sizeof(Type) - sizeof(int);
    db_fread(NodeScheme->buffer, NodeScheme->filledBuffer, 1, DB);
    return 1;
}

void cancelNode(memDB * DB, nodeStruct * NodeScheme) {
    NodeScheme->filledBuffer = 0;
    NodeScheme->created = 0;
}

int nextNode(memDB * DB, nodeStruct * NodeScheme) {
    cancelNode(DB, NodeScheme);
    if (NodeScheme->FirstElOffset == 0 || NodeScheme->lastElOffset == 0 ||
        NodeScheme->thisNodeOffset == 0)
        return 0;
    db_fseek(DB, NodeScheme->thisNodeOffset + sizeof(int) + sizeof(unsigned char), SEEK_SET);
    NodeScheme->prevNodeOffset = NodeScheme->thisNodeOffset;
    db_fread(&NodeScheme->thisNodeOffset, sizeof(int), 1, DB);
    return 1;
}

void rewindFirstNodes(memDB * DB, nodeStruct * NodeScheme) {
    cancelNode(DB, NodeScheme);
    NodeScheme->thisNodeOffset = NodeScheme->FirstElOffset;
    NodeScheme->prevNodeOffset = 0;
}

int deleteNode(memDB * DB, nodeStruct * NodeScheme) {
    unsigned char Type = fSpace;
    int AfterDeletedOffs;
    cancelNode(DB, NodeScheme);
    if (NodeScheme->FirstElOffset == 0 || NodeScheme->lastElOffset == 0 || NodeScheme->thisNodeOffset == 0)
        return 0;
    db_fseek(DB, NodeScheme->thisNodeOffset + sizeof(int), SEEK_SET);
    db_fwrite(&Type, sizeof(Type), 1, DB);
    db_fflush(DB);
    db_fread(&AfterDeletedOffs, sizeof(AfterDeletedOffs), 1, DB);
    if (NodeScheme->FirstElOffset == NodeScheme->lastElOffset) { 
        int EmptyOffset = 0;
        db_fseek(DB, NodeScheme->rootOffset, SEEK_SET);
        db_fwrite(&EmptyOffset, sizeof(EmptyOffset), 1, DB);
        NodeScheme->FirstElOffset = 0;
        db_fwrite(&EmptyOffset, sizeof(EmptyOffset), 1, DB);
        NodeScheme->lastElOffset = 0;
        NodeScheme->prevNodeOffset = 0;
        NodeScheme->thisNodeOffset = 0;
    } else if (NodeScheme->FirstElOffset == NodeScheme->thisNodeOffset) { 
        db_fseek(DB, NodeScheme->rootOffset, SEEK_SET);
        db_fwrite(&AfterDeletedOffs, sizeof(AfterDeletedOffs), 1, DB);
        NodeScheme->FirstElOffset = AfterDeletedOffs;
        NodeScheme->prevNodeOffset = 0;
        NodeScheme->thisNodeOffset = AfterDeletedOffs;
    } else if (NodeScheme->lastElOffset == NodeScheme->thisNodeOffset) { 
        int EmptyOffset = 0;
        db_fseek(DB, NodeScheme->prevNodeOffset + sizeof(int) + sizeof(unsigned char), SEEK_SET);
        db_fwrite(&EmptyOffset, sizeof(EmptyOffset), 1, DB);
        db_fseek(DB, NodeScheme->rootOffset + sizeof(int), SEEK_SET);
        db_fwrite(&NodeScheme->prevNodeOffset, sizeof(int), 1, DB);
        NodeScheme->lastElOffset = NodeScheme->prevNodeOffset;
        NodeScheme->prevNodeOffset = 0;
        NodeScheme->thisNodeOffset = 0;
    } else {
        db_fseek(DB, NodeScheme->prevNodeOffset + sizeof(int) + sizeof(unsigned char), SEEK_SET);
        db_fwrite(&AfterDeletedOffs, sizeof(AfterDeletedOffs), 1, DB);
        NodeScheme->thisNodeOffset = AfterDeletedOffs;
    }
    db_fflush(DB);
    return 1;
}


int createString(memDB * DB, char * S) {
    unsigned char Type = fString;
    int L = strlen(S);
    int n = sizeof(int) + sizeof(unsigned char) + 1 + L;
    int result;
    db_fseek(DB, 0, SEEK_END);
    result = db_ftell(DB);
    db_fwrite(&n, sizeof(n), 1, DB);
    db_fwrite(&Type, sizeof(Type), 1, DB);
    db_fwrite(S, L + 1, 1, DB);
    db_fflush(DB);
    return result;
}


char * getString(memDB * DB, int Offset) {
    unsigned char Type;
    char * result;
    int L;
    int n;
    db_fseek(DB, Offset, SEEK_SET);
    db_fread(&n, sizeof(n), 1, DB);
    db_fread(&Type, sizeof(Type), 1, DB);
    if (Type != fString)
        return NULL;
    L = n - sizeof(int) - sizeof(unsigned char);
    result = (char *) malloc(L);
    occupied_memory += L;
    db_fread(result, L, 1, DB);
    return result;
}

void setNodeAttr(memDB * DB, nodeStruct * NodeScheme, char * AttrName, float Value) {
    int n;
    if (NodeScheme->filledBuffer > 0 && findAttrByName(NodeScheme, AttrName, &n)) {
        n *= sizeof(float);
        write_buffer(NodeScheme->buffer, &n, Value);
    }
}

float getNodeAttr(memDB * DB, nodeStruct * NodeScheme, char * AttrName) {
    int n;
    if (NodeScheme->filledBuffer > 0 && findAttrByName(NodeScheme, AttrName, &n)) {
        float * buf = (float *) NodeScheme->buffer;
        return buf[n];
    } else
        return 0.0;
}
int LinkCurrentNodeToCurrentNode(memDB * DB, nodeStruct * NodeSchemeFrom, nodeStruct * NodeSchemeTo) {
    memoryAttr * AList = NodeSchemeFrom->firstFromAttrs;
    nodeConnection * DList = NodeSchemeFrom->connectFirst;
    float * buf;
    int n, i;
    int offs = 0;
    while (DList != NULL && DList->node != NodeSchemeTo) {
        DList = DList->next;
    }
    if (DList == NULL)
        return 0;
    while (AList != NULL) {
        offs += sizeof(float);
        AList = AList->next;
    }
    
    buf = (float *) (NodeSchemeFrom->buffer + offs);
    n = *buf;
    for (i = 0; i < n; i++)
        if (buf[2*i+1] == NodeSchemeTo->rootOffset && buf[2*i+2] == NodeSchemeTo->thisNodeOffset)
            return 1;
    if (n < reserved_for_links_in_node) {
        (*buf)++;
        buf[2*n+1] = NodeSchemeTo->rootOffset;
        buf[2*n+2] = NodeSchemeTo->thisNodeOffset;
        return 1;
    } else
        return 0;
}

void postNode(memDB * DB, nodeStruct * NodeScheme) {
    if (NodeScheme->filledBuffer > 0) {
        
        if (NodeScheme->created) {
            int n = sizeof(int) + sizeof(unsigned char) + sizeof(int) + NodeScheme->filledBuffer;
            unsigned char Type = fNode;
            int EmptyOffset = 0;
            db_fseek(DB, 0, SEEK_END);
            NodeScheme->thisNodeOffset = db_ftell(DB);
            db_fwrite(&n, sizeof(n), 1, DB);
            db_fwrite(&Type, sizeof(Type), 1, DB);
            db_fwrite(&EmptyOffset, sizeof(EmptyOffset), 1, DB);
            db_fwrite(NodeScheme->buffer, NodeScheme->filledBuffer, 1, DB);
            NodeScheme->prevNodeOffset = NodeScheme->lastElOffset;
            if (NodeScheme->FirstElOffset == 0 && NodeScheme->lastElOffset == 0) { 
                db_fseek(DB, NodeScheme->rootOffset, SEEK_SET);
                db_fwrite(&NodeScheme->thisNodeOffset, sizeof(int), 1, DB);
                db_fwrite(&NodeScheme->thisNodeOffset, sizeof(int), 1, DB);
                NodeScheme->FirstElOffset = NodeScheme->thisNodeOffset;
                NodeScheme->lastElOffset = NodeScheme->thisNodeOffset;
            } else {
                db_fseek(DB, NodeScheme->prevNodeOffset + sizeof(int) + sizeof(unsigned char), SEEK_SET);
                db_fwrite(&NodeScheme->thisNodeOffset, sizeof(int), 1, DB);
                db_fseek(DB, NodeScheme->rootOffset + sizeof(int), SEEK_SET);
                db_fwrite(&NodeScheme->thisNodeOffset, sizeof(int), 1, DB);
                NodeScheme->lastElOffset = NodeScheme->thisNodeOffset;
            }
            NodeScheme->created = 0;
        } else {
            db_fseek(DB, NodeScheme->thisNodeOffset + sizeof(int) + sizeof(unsigned char) + sizeof(int), SEEK_SET);
            db_fwrite(NodeScheme->buffer, NodeScheme->filledBuffer, 1, DB);
        }
        db_fflush(DB);
    }
}

int testNodeCondition(memDB * DB, nodeStruct * NodeScheme, memCondition * Condition) {

    memConditionOperand Left;
    memConditionOperand Right;
    int result;
    if (NodeScheme->filledBuffer == 0)
        return 0;
    if (Condition == NULL)
        return 1;

    Left = *Condition->Operand1;
    if (Condition->Operand1->OperandType == opdAttrName) {
        int i;
        memoryAttr * desc = findAttrByName(NodeScheme, Condition->Operand1->opAttrName, &i);
        float val = getNodeAttr(DB, NodeScheme, Condition->Operand1->opAttrName);
        switch (desc->type) {
            case aString:
                Left.OperandType = opdString;
                Left.opString = getString(DB, val);
                break;
            default:
                Left.OperandType = opdNumber;
                Left.opInt_Bool_Float = val;
                break;
        }
    }
    Right = *Condition->Operand2;
    if (Condition->Operand2->OperandType == opdAttrName) {
        int i;
        memoryAttr * desc = findAttrByName(NodeScheme, Condition->Operand2->opAttrName, &i);
        float val = getNodeAttr(DB, NodeScheme, Condition->Operand2->opAttrName);
        switch (desc->type) {
            case aString:
                Right.OperandType = opdString;
                Right.opString = getString(DB, val);
                break;
            default:
                Right.OperandType = opdNumber;
                Right.opInt_Bool_Float = val;
                break;
        }
    }

    switch (Condition->OperationType) {
        case opEqual: {
            if (Left.OperandType == Right.OperandType) {
                if (Left.OperandType == opdString)
                    result = strcmp(Left.opString, Right.opString) == 0;
                else
                    result = Left.opInt_Bool_Float == Right.opInt_Bool_Float;
            } else
                result = 0;
            break;
        }
        case opNotEqual: {
            if (Left.OperandType == Right.OperandType) {
                if (Left.OperandType == opdString)
                    result = strcmp(Left.opString, Right.opString) != 0;
                else
                    result = Left.opInt_Bool_Float != Right.opInt_Bool_Float;
            } else
                result = 0;
            break;
        }
        case opLess: {
            if (Left.OperandType == Right.OperandType) {
                if (Left.OperandType == opdString)
                    result = strcmp(Left.opString, Right.opString) < 0;
                else
                    result = Left.opInt_Bool_Float < Right.opInt_Bool_Float;
            } else
                result = 0;
            break;
        }
        case opGreater: {
            if (Left.OperandType == Right.OperandType) {
                if (Left.OperandType == opdString)
                    result = strcmp(Left.opString, Right.opString) > 0;
                else
                    result = Left.opInt_Bool_Float > Right.opInt_Bool_Float;
            } else
                result = 0;
            break;
        }
        case opNot: {
            result = !testNodeCondition(DB, NodeScheme, Left.opCondition);
            break;
        }
        case opAnd: {
            result = testNodeCondition(DB, NodeScheme, Left.opCondition) &&
                     testNodeCondition(DB, NodeScheme, Right.opCondition);
            break;
        }
        case opOr: {
            result = testNodeCondition(DB, NodeScheme, Left.opCondition) ||
                     testNodeCondition(DB, NodeScheme, Right.opCondition);
            break;
        }
    }

    if (Condition->Operand1->OperandType == opdAttrName && Left.OperandType == opdString) {
        occupied_memory -= 1 + strlen(Left.opString);
        free(Left.opString);
    }
    if (Condition->Operand2->OperandType == opdAttrName && Right.OperandType == opdString) {
        occupied_memory -= 1 + strlen(Right.opString);
        free(Right.opString);
    }
    return result;
}

memNodeSetItem * queryAllNodesOfType(memDB * DB, nodeStruct * NodeScheme, memCondition * Condition) {
    memNodeSetItem * result = NULL;
    memNodeSetItem * prev = NULL;
    rewindFirstNodes(DB, NodeScheme);
    while (openNode(DB, NodeScheme)) {
        if (testNodeCondition(DB, NodeScheme, Condition)) {
            memNodeSetItem * item = (memNodeSetItem *) malloc(sizeof(memNodeSetItem));
            occupied_memory += sizeof(memNodeSetItem);
            item->NodeScheme = NodeScheme;
            item->PrevOffset = NodeScheme->prevNodeOffset;
            item->ThisOffset = NodeScheme->thisNodeOffset;
            item->next = NULL;
            item->prev = prev;
            if (prev != NULL) prev->next = item;
            prev = item;
            if (result == NULL) result = item;
        }
        nextNode(DB, NodeScheme);
    }
    return result;
}


void navigateByNodeSetItem(memDB * DB, memNodeSetItem * NodeSet) {
    NodeSet->NodeScheme->prevNodeOffset = NodeSet->PrevOffset;
    NodeSet->NodeScheme->thisNodeOffset = NodeSet->ThisOffset;
}

memNodeSetItem * queryNodeSet(memDB * DB, memNodeSetItem * NodeSet, memCondition * Condition) {
    memNodeSetItem * result = NULL;
    memNodeSetItem * prev = NULL;
    if (NodeSet == NULL)
        return NULL;
    rewindFirstNodes(DB, NodeSet->NodeScheme);
    while (NodeSet != NULL && openNode(DB, NodeSet->NodeScheme)) {
        if (NodeSet->NodeScheme->thisNodeOffset == NodeSet->ThisOffset) {
            NodeSet->PrevOffset = NodeSet->NodeScheme->prevNodeOffset;
            if (testNodeCondition(DB, NodeSet->NodeScheme, Condition)) {
                memNodeSetItem * item = (memNodeSetItem *) malloc(sizeof(memNodeSetItem));
                occupied_memory += sizeof(memNodeSetItem);
                item->NodeScheme = NodeSet->NodeScheme;
                item->PrevOffset = NodeSet->NodeScheme->prevNodeOffset;
                item->ThisOffset = NodeSet->NodeScheme->thisNodeOffset;
                item->next = NULL;
                item->prev = prev;
                if (prev != NULL) prev->next = item;
                prev = item;
                if (result == NULL) result = item;
            }
            cancelNode(DB, NodeSet->NodeScheme);
            NodeSet = NodeSet->next;
        } else {
            cancelNode(DB, NodeSet->NodeScheme);
            nextNode(DB, NodeSet->NodeScheme);
        }
    }
    return result;
}

void freeNodeSet(memDB * DB, memNodeSetItem * NodeSet) {
    while (NodeSet != NULL) {
        memNodeSetItem * to_delete = NodeSet;
        NodeSet = NodeSet->next;
        occupied_memory -= sizeof(*to_delete);
        free(to_delete);
    }
}

float * getDirectedToList(memDB * DB, nodeStruct * NodeScheme, int * n) {
    memoryAttr * AList = NodeScheme->firstFromAttrs;
    float * buf;
    float * result = NULL;
    int offs = 0;
    while (AList != NULL) {
        offs += sizeof(float);
        AList = AList->next;
    }
    // число присоединенных узлов
    buf = (float *) (NodeScheme->buffer + offs);
    *n = *buf;
    if (*n != 0) {
        int i;
        int nbytes = 2*(*n)*sizeof(float);
        result = (float *) malloc(nbytes);
        occupied_memory += nbytes;
        for (i = 0; i < *n; i++) {
            unsigned char Type;
            
            db_fseek(DB, buf[2*i+2] + sizeof(int), SEEK_SET);
            db_fread(&Type, sizeof(Type), 1, DB);
            if (Type != fNode) {
                result[2*i] = 0.0;
                result[2*i+1] = 0.0;
            } else {
                result[2*i] = buf[2*i+1];
                result[2*i+1] = buf[2*i+2];
            }
        }
    }
    return result;
}

memNodeSetItem * _queryCypherStyle(memDB * DB, int nLinks, va_list args) {
    memNodeSetItem * set;
    nodeStruct * NodeScheme;
    memCondition * Condition;
    if (nLinks == 0)
        return NULL;
    NodeScheme = va_arg(args, nodeStruct *);
    Condition = va_arg(args, memCondition *);
    set = queryAllNodesOfType(DB, NodeScheme, Condition);
    nLinks--;
    while (nLinks > 0) {
        nodeStruct * NodeSchemeNext;
        memCondition * ConditionNext;
        memNodeSetItem * setNext0 = NULL;
        memNodeSetItem * setNext;
        memNodeSetItem * set_ptr;
        memNodeSetItem * prev = NULL;
        if (set == NULL)
            return NULL;
        NodeSchemeNext = va_arg(args, nodeStruct *);
        ConditionNext = va_arg(args, memCondition *);
        set_ptr = set;
        while (set_ptr != NULL) {
            NodeScheme->prevNodeOffset = set_ptr->PrevOffset;
            NodeScheme->thisNodeOffset = set_ptr->ThisOffset;
            if (openNode(DB, NodeScheme)) {
                int i, n;
                float * links = getDirectedToList(DB, NodeScheme, &n);
                for (i = 0; i < n; i++) {
                    if (links[2*i] == NodeSchemeNext->rootOffset) {
                        memNodeSetItem * item = (memNodeSetItem *) malloc(sizeof(memNodeSetItem));
                        occupied_memory += sizeof(memNodeSetItem);
                        item->NodeScheme = NodeSchemeNext;
                        item->PrevOffset = 0;
                        item->ThisOffset = links[2*i+1];
                        item->next = NULL;
                        item->prev = prev;
                        if (prev != NULL) prev->next = item;
                        prev = item;
                        if (setNext0 == NULL) setNext0 = item;
                    }
                }
                cancelNode(DB, NodeScheme);
                occupied_memory -= 2*n*sizeof(float);
                free(links);
            }
            set_ptr = set_ptr->next;
        }
        if (setNext0 == NULL)
            setNext = NULL;
        else
            setNext = queryNodeSet(DB, setNext0, ConditionNext);
        freeNodeSet(DB, set);
        freeNodeSet(DB, setNext0);
        set = setNext;
        NodeScheme = NodeSchemeNext;
        Condition = ConditionNext;
        nLinks--;
    }
    return set;
}

memNodeSetItem * queryCypherStyle(memDB * DB, int nLinks, ...) {
    memNodeSetItem * result;
    va_list args;
    va_start(args, nLinks);
    result = _queryCypherStyle(DB, nLinks, args);
    va_end(args);
    return result;
}

void deleteCypherStyle(memDB * DB, int nLinks, ...) {
    memNodeSetItem * set;
    memNodeSetItem * set1;
    va_list args;
    va_start(args, nLinks);
    set = _queryCypherStyle(DB, nLinks, args);
    set1 = set;
    va_end(args);
    while (set != NULL && set->next != NULL)
        set = set->next;
    while (set != NULL) {
        navigateByNodeSetItem(DB, set);
        deleteNode(DB, set->NodeScheme);
        set = set->prev;
    }
    freeNodeSet(DB, set1);
}

void setCypherStyle(memDB * DB, char * AttrName, float AttrVal, int nLinks, ...) {
    memNodeSetItem * set;
    memNodeSetItem * set1;
    va_list args;
    va_start(args, nLinks);
    set = _queryCypherStyle(DB, nLinks, args);
    set1 = set;
    va_end(args);
    while (set != NULL) {
        navigateByNodeSetItem(DB, set);
        if (openNode(DB, set->NodeScheme)) {
            setNodeAttr(DB, set->NodeScheme, AttrName, AttrVal);
            postNode(DB, set->NodeScheme);
        }
        set = set->next;
    }
    freeNodeSet(DB, set1);
}

memCondition * createLogicCondition(unsigned char operation, memCondition * operand1, memCondition * operand2) {
    memCondition * result = (memCondition *) malloc(sizeof(memCondition));
    memConditionOperand * _operand1 = (memConditionOperand *) malloc(sizeof(memConditionOperand));
    memConditionOperand * _operand2 = (memConditionOperand *) malloc(sizeof(memConditionOperand));
    occupied_memory += sizeof(memCondition) + 2*sizeof(memConditionOperand);
    _operand1->OperandType = opdCond;
    _operand1->opCondition = operand1;
    _operand2->OperandType = opdCond;
    _operand2->opCondition = operand2;
    result->OperationType = operation;
    result->Operand1 = _operand1;
    result->Operand2 = _operand2;
    return result;
}


memCondition * createStringAttrCondition(unsigned char operation, char * AttrName, char * Val) {
    memCondition * result = (memCondition *) malloc(sizeof(memCondition));
    memConditionOperand * operand1 = (memConditionOperand *) malloc(sizeof(memConditionOperand));
    memConditionOperand * operand2 = (memConditionOperand *) malloc(sizeof(memConditionOperand));
    occupied_memory += sizeof(memCondition) + 2*sizeof(memConditionOperand);
    operand1->OperandType = opdAttrName;
    operand1->opAttrName = (char *) malloc((strlen(AttrName)+1)*sizeof(char));
    occupied_memory += 1 + strlen(AttrName);
    strcpy(operand1->opAttrName, AttrName);
    operand2->OperandType = opdString;
    operand2->opString =  (char *) malloc((strlen(Val)+1)*sizeof(char));
    occupied_memory += 1 + strlen(Val);
    strcpy(operand2->opString, Val);
    result->OperationType = operation;
    result->Operand1 = operand1;
    result->Operand2 = operand2;
    return result;
}

memCondition * createIntOrBoolAttrCondition(unsigned char operation, char * AttrName, int Val) {
    return createFloatAttrCondition(operation, AttrName, (float) Val);
}

memCondition * createFloatAttrCondition(unsigned char operation, char * AttrName, float Val) {
    memCondition * result = (memCondition *) malloc(sizeof(memCondition));
    memConditionOperand * operand1 = (memConditionOperand *) malloc(sizeof(memConditionOperand));
    memConditionOperand * operand2 = (memConditionOperand *) malloc(sizeof(memConditionOperand));
    occupied_memory += sizeof(memCondition) + 2*sizeof(memConditionOperand);
    operand1->OperandType = opdAttrName;
    operand1->opAttrName = (char *) malloc((strlen(AttrName)+1)*sizeof(char));
    occupied_memory += 1 + strlen(AttrName);
    strcpy(operand1->opAttrName, AttrName);
    operand2->OperandType = opdNumber;
    operand2->opInt_Bool_Float = Val;
    result->OperationType = operation;
    result->Operand1 = operand1;
    result->Operand2 = operand2;
    return result;
}

void freeOperand(memConditionOperand * op) {
    switch (op->OperandType) {
        case opdCond:
            freeCondition(op->opCondition);
            break;
        case opdString:
            occupied_memory -= 1 + strlen(op->opString);
            free(op->opString);
            break;
        case opdAttrName:
            occupied_memory -= 1 + strlen(op->opAttrName);
            free(op->opAttrName);
            break;
    }
    occupied_memory -= sizeof(*op);
    free(op);
}

void freeCondition(memCondition * Cond) {
    freeOperand(Cond->Operand1);
    freeOperand(Cond->Operand2);
    occupied_memory -= sizeof(*Cond);
    free(Cond);
}