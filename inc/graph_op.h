#ifndef GRAPH_OP_H
#define GRAPH_OP_H
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "node_graph.h"

/*поддерживаемые типы данных - (поддержать тривиальные значения по меньшей мере следующих типов: четырёхбайтовые
целые числа и числа с плавающей точкой, текстовые строки произвольной длины, булевские
значения)*/

enum
{
    fSpace = 0,
    fString,
    fNode
} typesFileData;

enum
{
    aInt32 = 0,
    aFloat,
    aString,
    aBool
} typesAttr;

//структуры для определения запроса и его условий
enum 
{
    opdNumber = 0,
    opdString,
    opdAttrName,
    opdCond
};

enum
{
    opEqual = 0,
    opNotEqual, 
    opLess, 
    opGreater, 
    opNot, 
    opAnd, 
    opOr
};
#endif //GRAPH_OP_H