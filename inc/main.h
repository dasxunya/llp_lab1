// Database operations
#ifndef MAIN_H
#define MAIN_H
#include "../inc/graph_op.h"
#include "../inc/test_data.h"
#define nMovies 5
#define max_rand_str_length 99

#define RESET "\033[0m"
#define WHITE "\033[1;37m"

double seconds()
{
    return (1.0 * clock() / CLOCKS_PER_SEC);
}

char *gen_rand_str()
{
    int n = rand() % (max_rand_str_length + 1);
    char *result = (char *)malloc(n + 1);
    int i;
    for (i = 0; i < n; i++)
        result[i] = 'A' + (rand() % ('Z' - 'A' + 1));
    result[n] = 0;
    return result;
}
#endif // MAIN_H
