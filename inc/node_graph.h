#ifndef NODE_GRAPH_H
#define NODE_GRAPH_H

struct memoryAttr;
struct nodeStruct;
struct memCondition;

// допустимые типы для связи с узлом
typedef struct nodeConnection
{
    struct nodeStruct *node;
    struct nodeConnection *next;
} nodeConnection;

// структура узла
typedef struct nodeStruct
{
    char *nameType;
    char *buffer;
    int filledBuffer;
    int created;
    int rootOffset;
    int FirstElOffset;
    int lastElOffset;
    int prevNodeOffset;
    int thisNodeOffset;
    nodeConnection *connectFirst; // начало списка возможных типов узлов
    nodeConnection *connectLast;  // конец списка возможных типов узлов
    struct memoryAttr *firstFromAttrs;
    struct memoryAttr *lastFromAttrs;
    struct nodeStruct *NextNodeStruct;
} nodeStruct;

typedef struct memoryAttr
{
    char *attrName;
    unsigned char type;
    struct memoryAttr *next;
} memoryAttr;

// структура БД
typedef struct
{
    nodeStruct *FirstSchemeNode; // указатель на начало
    nodeStruct *LastSchemeNode;  // указатель на конец
} memDBScheme;

// открытая БД
typedef struct
{
    memDBScheme *Scheme;
    char *WriteBuffer;
    int nWriteBuffer;
    char *ReadBuffer;
    int nReadBuffer;
    int iReadBuffer;
    FILE *FileDB;
} memDB;

// хранит результат запроса к БД
typedef struct memNodeSetItem
{
    nodeStruct *NodeScheme;
    int PrevOffset;
    int ThisOffset;
    struct memNodeSetItem *next;
    struct memNodeSetItem *prev;
} memNodeSetItem;

// операнд в условии
typedef struct
{
    unsigned char OperandType;
    union
    {
        struct memCondition *opCondition;
        char *opString; // строка
        float opInt_Bool_Float;
        char *opAttrName;
    };
} memConditionOperand;

// элемент входящий в условие
typedef struct memCondition
{
    unsigned char OperationType;
    memConditionOperand *Operand1;
    memConditionOperand *Operand2;
} memCondition;

void initGraphsData(char *configFileName);

memDBScheme *createScheme();
void freeDBSchemeAttr(memoryAttr *Attr);
void freeDBSchemeNode(nodeStruct *NodeScheme);
void freeDBScheme(memDBScheme *Scheme);

nodeStruct *findNodeSchemeByTypeName(memDBScheme *Scheme, char *TypeName, int *n);

nodeStruct *addNodeTypeToScheme(memDBScheme *Scheme, char *TypeName);

void delNodeTypeFromScheme(memDBScheme *Scheme, nodeStruct *NodeScheme);

nodeConnection *checkCanLinkTo(nodeStruct *NodeScheme, nodeStruct *ToNodeScheme);
nodeConnection *addDirectedToNodeScheme(nodeStruct *NodeScheme, nodeStruct *ToNodeScheme);

void delDirectedToFromNodeType(nodeStruct *NodeScheme, nodeStruct *Deleting);

memoryAttr *findAttrByName(nodeStruct *NodeScheme, char *Name, int *n);
memoryAttr *addAttrToNodeScheme(nodeStruct *NodeScheme, char *Name, unsigned char Type);

void delAttrFromNodeScheme(nodeStruct *NodeScheme, memoryAttr *Deleting);

memDB *createNew(memDBScheme *Scheme, char *FileName);
memDB *openDB(char *FileName);

void closeDB(memDB *DB);
void rewindFirstNodes(memDB *DB, nodeStruct *NodeScheme);

int nextNode(memDB *DB, nodeStruct *NodeScheme);
int openNode(memDB *DB, nodeStruct *NodeScheme);

void createNode(memDB *DB, nodeStruct *NodeScheme);
void cancelNode(memDB *DB, nodeStruct *NodeScheme);

int deleteNode(memDB *DB, nodeStruct *NodeScheme);

void setNodeAttr(memDB *DB, nodeStruct *NodeScheme, char *AttrName, float Value);

float getNodeAttr(memDB *DB, nodeStruct *NodeScheme, char *AttrName);

int createString(memDB *DB, char *S);

char *getString(memDB *DB, int Offset);

float *getDirectedToList(memDB *DB, nodeStruct *NodeScheme, int *n);

void setNodeAttr(memDB *DB, nodeStruct *NodeScheme, char *AttrName, float Value);

float getNodeAttr(memDB *DB, nodeStruct *NodeScheme, char *AttrName);

int LinkCurrentNodeToCurrentNode(memDB *DB, nodeStruct *NodeSchemeFrom, nodeStruct *NodeSchemeTo);

void postNode(memDB *DB, nodeStruct *NodeScheme);

memCondition *createLogicCondition(unsigned char operation, memCondition *operand1, memCondition *operand2);
memCondition *createStringAttrCondition(unsigned char operation, char *AttrName, char *Val);
memCondition *createIntOrBoolAttrCondition(unsigned char operation, char *AttrName, int Val);
memCondition *createFloatAttrCondition(unsigned char operation, char *AttrName, float Val);

void freeOperand(memConditionOperand *op);
void freeCondition(memCondition *Cond);

int testNodeCondition(memDB *DB, nodeStruct *NodeScheme, memCondition *Condition);

memNodeSetItem *queryAllNodesOfType(memDB *DB, nodeStruct *NodeScheme, memCondition *Condition);
memNodeSetItem *queryNodeSet(memDB *DB, memNodeSetItem *NodeSet, memCondition *Condition);
memNodeSetItem *queryCypherStyle(memDB *DB, int nLinks, ...);

void navigateByNodeSetItem(memDB *DB, memNodeSetItem *NodeSet);
void deleteCypherStyle(memDB *DB, int nLinks, ...);
void setCypherStyle(memDB *DB, char *AttrName, float AttrVal, int nLinks, ...);

int getOccupiedMemory();
void register_free(int amount);

void freeNodeSet(memDB *DB, memNodeSetItem *NodeSet);
#endif
