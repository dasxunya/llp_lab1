all: main

main: main.o test_data.o
	gcc main.o test_data.o -o main

main.o:
	gcc -c src/main.c

test_data.o:
	gcc -c src/test_data.c

exec:
	./main

clean:
	rm -f *.o; \
	rm main.exe
